#include "Header.h"

using namespace std;

Food CreateNewFood(int lastId)
{
	string name;
	float price;
	cout << "Enter name of new food: ";
	cin >> name;
	cout << "Enter price of new food: ";
	cin >> price;
	Food v(lastId++, name, price);
	cout << "Done." << endl << endl;
	system("pause");
	return v;
}