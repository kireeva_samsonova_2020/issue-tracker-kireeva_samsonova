#include "Header.h"

using namespace std;

void PrintAllFood(vector<Food> food)
{
	cout << "=== [All food] ===" << endl;
	for (auto v : food)
	{
		v.printInfo();
	}
	cout << endl;
}
