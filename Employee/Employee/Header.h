#pragma once
#include "File.h"
#include "Human.h"
#include "Food.h"
#include <string>
#include <iostream>
#include <iomanip>
#include <vector>

void DrawMenu(vector<string>);
void PrintAllHumans(vector<Human>);
void PrintAllFood(vector<Food>);
Food CreateNewFood(int);
Human CreateNewHuman(int);
void Order(vector<Human>&, vector<Food>&);