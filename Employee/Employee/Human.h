#pragma once
#include <string>
#include "Food.h"
#include <vector>

using namespace std;

class Human
{
private:
	int id;	// �������������
	int vId; // ������������� ���
	string postName;
	string name; // ��� 
	Food foods; // ��� ��� ��������
	vector<string> basket; // �������
	vector<int> price;
	vector<string> hBasket;
	vector<int> hPrice;
public:
	Human(); // ������ �����������
	Human(int, string, int); // �����������, ������� ����� ������������
	void setFood(Food); // ���������� ���
	void confirmOrder(); //������� � �������
	void printInfo(); // ����� ���� ����������
	void printInfoInBasket(); // ����� ���������� � ��� � �������
	string getName(); // ��������� �����
	int getId(); // �������� �������������
	int getFoodId(); // �������� �������������
	void clearBasket(); // �������� �������
	void printH(); //����� �������
	string getFoodHName(int);
	int getFoodPrice(int);
	vector <string> getLength();
};