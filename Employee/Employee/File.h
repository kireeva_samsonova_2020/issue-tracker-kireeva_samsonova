#pragma once
#include <string>
#include "Human.h"
#include <vector>
#include "Food.h"

using namespace std;

class File
{
private:
	string hPath = "humans.txt"; // ���� ��� �������� �� � ������
	string vPath = "food.txt"; // �� � ����
	string fhPath = "foodH.txt"; // �� �������� ���������� �������
public:
	File(); // �����������
	vector<Food> getAllFood(); // ��������� ���� ���
	vector<Human> getAllHumans(); // ��������� ���� �����
	void saveHumansData(vector<Human>); // ��������� �����
	void saveFoodData(vector<Food>); // ��������� ���
	void saveFoodHistori(vector<Human>); // ��������� ������� ���
	void getAndUpdateHFood(vector<Human>&); // �������� � ����������� ��� � ��������
};