#include "Food.h"
#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

Food::Food()  // ����������� 
{
	id = -1;
	foodName = "";
	foodPrice = 0;
}

Food::Food(int id, string foodName, float foodPrice) // ����������� � �����������
{
	this->id = id;
	this->foodName = foodName;
	this->foodPrice = foodPrice;
}

void Food::printInfo() // �������� ����������
{
	cout << "ID: " << id << setw(8) << " | Name of food: " << foodName << setw(8) << " | Price of food: " << foodPrice << endl;
}

string Food::getFoodName() // �������� �������� ���
{
	return foodName;
}

int Food::getFoodPrice() // �������� ���� ���
{
	return foodPrice;
}

int Food::getId() // �������� �������������
{
	return id;
}

