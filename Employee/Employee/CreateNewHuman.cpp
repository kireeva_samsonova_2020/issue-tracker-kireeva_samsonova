#include "Header.h"

Human CreateNewHuman(int lastId)
{
	string name;
	cout << "Enter name of new human: ";
	cin >> name;
	Human h(lastId++, name, -1);
	cout << "Done." << endl << endl;
	system("pause");
	return h;
}