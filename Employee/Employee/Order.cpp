#include "Header.h"

using namespace std;

void Order(vector<Human>& humans, vector<Food>& foods)
{
	PrintAllHumans(humans);
	cout << endl;
	cout << "Enter human ID: ";
	int id;
	cin >> id;
	if (id < humans.size())
	{
		cout << "The person who wants to order: " << humans[id].getName() << endl;
		vector<string> menu = { "Order", "Basket", "Last order" };
		int key;
		do
		{
			DrawMenu(menu);
			cout << endl << "Enter key or 0 to back, 4 to confirm order: ";
			cin >> key;
			switch (key)
			{
			case 1:
			{
				PrintAllFood(foods);
				cout << endl << "Enter foods id: ";
				int vId = -1;
				cin >> vId;
				if (vId < foods.size())
				{
					humans[id].setFood(foods[vId]);
					cout << "Done!" << endl;
					system("pause");
				}
				else
				{
					cout << "Incorrect id.";
				}
				break;
			}
			case 2:
			{
				humans[id].Human::printInfoInBasket();
				break;
			}
			case 3:
			{
				humans[id].Human::printH();
				break;
			}
			case 4:
			{
				humans[id].Human::confirmOrder();
				break;
			}
			case 0:
			{
				humans[id].Human::clearBasket();
				break;
			}
			}
		} while ((key != 0) && (key != 4));
	}
}