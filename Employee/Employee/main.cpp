﻿#include "Header.h"

using namespace std;

int main()
{
	setlocale(LC_ALL, "");
	vector <Food> foods; // пустой вектор для food
	vector <Human> humans; // пустой вектор для людей
	File file;	// объект класса файл
	cout << "Retrieving information from a database of foods..." << endl;
	foods = file.getAllFood(); // получаем инфу о еде
	cout << "Count of foods: " << foods.size() << endl << endl;
	cout << "Retrieving information from a database of humans..." << endl;
	humans = file.getAllHumans(); // получаем людей
	cout << "Count of humans: " << humans.size() << endl << endl;
	vector<string> menu = { "List of foods", "List of humans", "Create new foods", "Create new human", "Order food", "Save data" };
	file.getAndUpdateHFood(humans);
	int listItem;
	do
	{
		DrawMenu(menu);
		cout << endl << "Enter item number or 0 to exit: ";
		cin >> listItem;
		switch (listItem)
		{
		case 1:
			{
				PrintAllFood(foods);
				system("pause");
				break;
			}
		case 2:
			{
				PrintAllHumans(humans);
				system("pause");
				break;
			}
		case 3:
			foods.push_back(CreateNewFood(foods.size()));
			break;
		case 4:
			humans.push_back(CreateNewHuman(humans.size()));
			break;
		case 5:
			Order(humans, foods);
			break;
		case 6:
			{
				file.saveHumansData(humans);
				file.saveFoodData(foods);
				file.saveFoodHistori(humans);
				system("pause");
				break;
			}
		}
		system("cls");
	} while (listItem != 0);
}