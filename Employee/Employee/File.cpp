#include "File.h"
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

void File::saveHumansData(vector<Human> humans) // ���������� �����
{
	ofstream file(hPath); // ��������� ���� � �� � ������ ������
	for (auto human : humans) // ���������� ������ �����
	{
		file << human.getId() << " " << human.getName() << " " << human.getFoodId() << endl; // ���������� � ����
	}
	file.close(); // �������� ����
}

void File::saveFoodData(vector<Food> foods) // ���������� 
{
	ofstream file(vPath); // ��������� �� � ������ ������
	for (auto food : foods) // ���������� ��� ���
	{
		file << food.getId() << " " << food.getFoodName() << " " << food.getFoodPrice() << endl; // ���������� � ����
	}
	file.close(); // ���������
}

void File::saveFoodHistori(vector<Human> humans)
{
	ofstream file(fhPath); // ��������� �� � ������ ������
	for (int i = 0; i < humans.size(); i++)
	{
		for (int j = 0; j < humans[i].getLength().size(); j++)
		{
			file << humans[i].getId() << " " << humans[i].getFoodHName(j) << " " << humans[i].getFoodPrice(j) << endl;
		}
	}
	file.close();
}

void File::getAndUpdateHFood(vector<Human>& humans)
{
	ifstream file; // ��������� �� � ������ ������
	file.open(fhPath);
	string s;
	getline(file, s);
	file.close();
	if (s.empty())
	{
		return;
	}
	file.open(fhPath);
	string name;
	int price;
	int id;
	file >> id >> name >> price;
	int fid = id;
	while (!file.eof())
	{
		if (name.length())
		{
			Food food(id, name, price);
			humans[id].setFood(food);
		}
		if (fid != id)
		{
			humans[fid].confirmOrder();
			fid = id;
		}
		file >> id >> name >> price;
	}
	humans[id].confirmOrder();
	file.close();
}

vector<Food> File::getAllFood() // ��������� ���
{
	vector<Food> result; // ������� ������ ������
	ifstream file(vPath); // ��������� �� � ������ ������
	while (!file.eof()) // ���� �� ��������� ����� �����
	{
		string name;
		int price;
		int id;
		file >> id >> name >> price; // ������
		if (name.length())
		{
			Food that(id, name, price); // ������� ������ ������ Food � ������� �����������
			result.push_back(that); // ��������� � ������
		}
	}
	file.close();
	return result; // ���������� ������
}

vector<Human> File::getAllHumans() // ���������� ������� ����
{
	vector<Human> result;
	ifstream file(hPath);
	while (!file.eof())
	{
		string name;
		int id, vid;
		file >> id >> name >> vid;
		if (name.length())
		{
			Human that(id, name, vid);
			result.push_back(that);
		}
	}
	file.close();
	return result;
}

File::File() // �����������
{
	ifstream file; // ������ �����
	file.open(hPath, ios::app); // ������� (���� �� ������) 
	file.close(); // ���������
	file.open(vPath, ios::app); // ����������
	file.close();
	file.open(fhPath, ios::app); // ����������
	file.close();
}