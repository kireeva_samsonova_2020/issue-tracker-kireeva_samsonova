#include "Human.h"
#include "Food.h"
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>

using namespace std;

Human::Human() // �����������
{
	id = 0;
	postName = "";
	name = "";
	vId = 0;
}

Human::Human(int id, string name, int vId) // ����������� � �����������
{
	this->id = id;
	this->name = name;
	this->vId = vId;
	postName = "";
}

void Human::setFood(Food food) // ��������� ���������� ���
{
	vId = food.getId();
	this->foods = food;
	postName = food.getFoodName();
	basket.push_back(food.getFoodName());
	price.push_back(food.getFoodPrice());
}

void Human::confirmOrder()
{
	hBasket = basket;
	hPrice = price;
	clearBasket();
}

void Human::printInfo()
{
	cout << "ID: " << id << setw(4) << " | Name: " << name << endl;
}

void Human::printInfoInBasket()
{
	cout << "Cast consists:" << endl;
	for (int i = 0; i < basket.size(); i++)
		cout << "Name food: " << basket[i] << "| Price food: " << price[i] << endl;
}

void Human::clearBasket()
{
	basket.clear();
	price.clear();
}

void Human::printH()
{
	cout << "Last orders:" << endl;
	for (int i = 0; i < hBasket.size(); i++)
		cout << "Name food: " << hBasket[i] << "| Price food: " << hPrice[i] << endl;
}

int Human::getFoodPrice(int i)
{
	return hPrice[i];
}

string Human::getFoodHName(int i)
{
	return hBasket[i];
}

vector <string> Human::getLength()
{
	return hBasket;
}

string Human::getName()
{
	return name;
}

int Human::getId()
{
	return id;
}

int Human::getFoodId()
{
	return vId;
}